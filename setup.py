import os
from setuptools import setup

def read(fname):
    if os.path.exists(fname):
        return open(os.path.join(os.path.dirname(__file__), fname)).read()


srcdir = os.path.join("src")



setup(
    name="ubot",
    version="0.0.1",
    author="Marian Neagul",
    author_email="mneagul@gmail.com",
    description="Bot for doing various stuff",
    license="APL",
    keywords="chat",
    url="http://blog.segfault.ro",
    package_dir={'': srcdir},
    packages=["ubot", "ubot.ingestors"],
    long_description=read('README.md'),
    classifiers=[
        "License :: OSI Approved :: Apache Software License",
    ],
    entry_points={
        'console_scripts': [
            'ubot = ubot.cli:main',
            'ubot-send = ubot.cli:client'
        ],
        'ubot.input': [
            'zmq = ubot.ingestors.zeromq:ZeroMqInput [ZMQ]'
        ]
    },
    include_package_data = True,
    package_data = {
        'ubot': ['data/*.yaml', ],
    },
    install_requires=["pyyaml", "sleekxmpp>=1.3", "dnspython"],
    extras_require = {
        'ZMQ': ["pyzmq", ]
    },
)