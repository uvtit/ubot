ubot - The notification bot
===========================

The goal of ubot is to provide XMPP messaging capabilities for the UVT IT Teams.

The ubot requires the following software libraries:

- pyyaml
- sleekxmpp
- dnspython
- pyzmq

pyzmq is needed for interacting with clients (eg. nagios) using independent tools.

Installing
----------

Instalation should be fairly simple:

    :::bash
    export UBOTPREFIX=/opt/ubot/
    virtualenv --system-site-packages $UBOTPREFIX
    git clone https://bitbucket.org/uvtit/ubot.git
    cd ubot; $UBOTPREFIX/bin/python setup.py install
    cp ubot.yaml.sample ; /etc/ubot.yaml
    cp upstart/ubot.conf ; /etc/init/ # In case of ubuntu.


At this point ubot should be installed in /opt/ubot/.
Remember to modify the config file and provide the proper credentials.

Using
-----

`$UBOTPREFIX/bin/ubot-send` is a small tool that can send messages to the ubot.





