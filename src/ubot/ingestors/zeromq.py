# -*- coding: utf-8 -*-
'''
Copyright 2015 Marian Neagul

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author: Marian Neagul <neagul@gmail.com>
@contact: marian@info.uvt.ro
@copyright: 2015 Marian Neagul
'''

import zmq
import time
import threading
import logging


class ZeroMqInput(threading.Thread):
    def __init__(self, name, url, dispatcher, **extra):
        threading.Thread.__init__(self)
        self._input_name = name
        self._url = url
        self._extra = extra
        self.setName("input:%s" % name)
        self.log = logging.getLogger(__name__)
        self.log.debug("ZeroMQ Handler for %s instantiated!", self._url)
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.REP)
        self.dispatcher = dispatcher

    def run(self):
        log = self.log
        socket = self.socket
        self.log.info("binding to %s", self._url)
        socket.bind(self._url)
        self.log.info("Handling messages")
        while True:
            result = {'status': False}
            try:
                try:
                    in_msg = socket.recv_json()
                except:
                    self.log.exception("Error handling incoming message")
                    continue

                if u"to" in in_msg:
                    self.dispatcher.send_message_to_jid(in_msg["account"], in_msg["to"], in_msg.get("message", "<MISSING MESSAGE>"))
                else:
                    self.log.warning("Missing destination information for incoming message")
            finally:
                socket.send_json(result)

